import React from 'react';
import styled from 'styled-components';

const ExLink = styled.img`
    margin: 20px auto 0 7%;
    transition: transform 0.4s ease;

    :hover, :focus {
        transform: scale(1.05);
    }

    @media all and (max-width: 900px){
        margin: auto;
    }
`;

const exLink = props => (
    <a href={props.link} target="_blank" title={props.title} rel="noopener noreferrer">
        <ExLink src={props.image} style={{width: props.width}} alt={props.title}/>
    </a>
);

export default exLink;