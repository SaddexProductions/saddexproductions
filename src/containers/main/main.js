import React, { Fragment, useState, useEffect, useRef } from 'react';
import { Link, Switch, Route, withRouter } from 'react-router-dom';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';
import { months, isInViewport, convertError } from '../../shared/utility';

//components
import H1 from '../../components/UI/headline/headline';
import Gallery from '../../components/frontpageShowGallery/showGallery';
import Wrapper from '../../components/UI/wrapper/wrapper';
import Image from '../../components/UI/img/img';
import FlexContainer from '../../components/flexContainer/flexContainer';
import FlexElement from '../../components/flexContainer/flexInner/flexEl';
import Spinner from '../../components/UI/spinner/spinner';
import P from '../../components/standardParagraph/standardParagraph';
import Popup from '../../components/popup/popup';
import Modal from '../../components/UI/modal/modal';
import ErrorText from '../../components/UI/modal/errorText/errorText';
import Button from '../../components/UI/button/button';
import BodyErrorText from '../../components/UI/bodyErrorText/bodyErrorText';
import ModalButtonHolder from '../../components/UI/modal/errorModalButtonHolder/errorModalButtonHolder';

const Main = props => {

    //variables, constants, useState

    const { end, fetchProjects, error, modalOpen, modalClose, projects, wW, loading, resetError } = props;

    const images = [
        {
            src: "/img/me.jpg",
            top: "20px",
            _id: "me.jpg"
        }
    ];

    let projectContent = (
        <div className="placeHolder" style={{width: "100%", height: "30px"}}>
            <h4 style={{textAlign: "center"}}>Scroll to load</h4>
        </div>
    );

    let projectStatus = "";

    const [imageOffSet, setImageOffset] = useState(0);
    const [scrolledOnce, setSTBO] = useState(false);
    const [errorMsg, setErrorMsg] = useState(null);

    const el = useRef(null);   

    useEffect(() => {
        
        if (scrolledOnce && projects.length === 0 && !loading && !error) {
            fetchProjects(0);
        }

        return () => resetError()

    }, [fetchProjects, scrolledOnce, projects, resetError]);

    //usEffect

    useEffect(() => {
        if (error) {
            modalOpen();
            setErrorMsg(convertError(error));
        }
        else {
            modalClose();
        }
    }, [error, modalOpen, modalClose]);

    //methods

    const triggerLoadHandler = height => {
        if(height-50 < window.innerHeight){
            setSTBO(true);
        }
    }

    const offSetImage = (e) => {
        setImageOffset(e.target.scrollTop);
    }

    //modals

    if (loading) {
        projectStatus = <Spinner />;
    }

    if (error) {
        projectStatus = <BodyErrorText>{errorMsg}</BodyErrorText>;
    }

    if ((!projects.length && error) || (!projects.length && loading)) {

        projectContent = "";
    }

    if (projects.length && Array.isArray(projects)) {
        projectContent = (
            <Fragment>
                {projects.map((g, index) => {
                    return <Gallery
                        key={g.title}
                        image={g.image}
                        offset={(imageOffSet * .2) - ((index+2)*70)}
                        title={g.title}
                        type={g.type}
                        front={g.frontEnd}
                        back={g.backEnd}
                        dOC={months.english[parseInt(g.yMoC.time.split('-')[1]) - 1] + " " + g.yMoC.time.split('-')[0] + (g.yMoC.workInProgress ? " (work in progress)" : "")}
                        url={g.url}
                    />
                })}
                {end ? '' : <div style={{display: 'flex', justifyContent: 'center', marginBottom: '30px'}}>
                    <Button clicked={() => fetchProjects(projects.length)}>Load more...</Button>
                </div>}
            </Fragment>
        );
    }

    const d1 = Date.now();
    const d2 = new Date(1996, 4, 16).getTime();

    const age = Math.floor((d1 - d2) / (1000 * 3600 * 24 * 365.25));

    return (
        <Fragment>
            <Helmet>
                <title>Saddex Productions</title>
            </Helmet>
            <Switch>
                <Route path="/popup"
                    render={() => <Popup {...props} items={images} type="image" />}
                />
            </Switch>
            <Modal enabled={props.isModalOpen}>
                <ErrorText><FontAwesomeIcon style={{ display: error != null ? "inline" : "none" }} icon={faExclamationTriangle} />
                    {error != null ? `  ${errorMsg}` : null}
                </ErrorText>
                <p style={{ textAlign: "center", fontWeight: "bold" }}>Could not load projects!</p>
                <ModalButtonHolder err={error}>
                    <Button clicked={() => { modalClose() }}>Dismiss</Button>
                    <Button clicked={() => { fetchProjects(projects.length) }}>Retry</Button>
                </ModalButtonHolder>
            </Modal>
            <Wrapper scrolled={e => {
                const isVisible = isInViewport(el.current);
                if (!scrolledOnce && isVisible) {
                    setSTBO(true);
                }
                offSetImage(e);
            }} 
            enableScroll 
            locked={props.isModalOpen}
            passHeight={triggerLoadHandler}
            >
                <FlexContainer>
                    {/* Text content will be fetched as Markdown from DB in the future */}
                    <FlexElement>
                        <h2>Who am I?</h2>
                        <p>
                            My name is Martin and I am a {age}-year old
                            full-stack developer from northern Sweden, currently living in Härnösand.
                            I build applications primarily on the MongoDB-Express-NodeJS backend stack, but can utilise any of
                            the big three frameworks (ReactJS, Angular 2+, VueJS), with the greatest fluency in React which
                            I also like to use together with Redux state management and unit testing.
                            <br/>
                            The skills I possess have to a large extent been aquired through self-teaching, which include online courses
                            and putting the things I have learned to the test by building projects like those listed below on this page.
                            I like to challenge myself and try not only to learn new technologies when I can, but also by building
                            innovative solutions - like for example implementing 2FA in my projects.
                            I also have some knowledge in building applications with React and Django (DRF),
                            or just Django,. The languages I know are primarily Javascript/Typescript, HTML, CSS and Python, 
                            but I also know some SQL and C, and some C# from years ago. Additionally, I also learn Java at the moment and know the basics of it, 
                            even if I still need to learn how to use it practically in for example Android development.
                            <br/>
                            I also have plenty of skills in <Link to="/videos">video/image editing, 3D and compositing
                            </Link> which I could utilise to design neat websites.
                            Profile on the <Link to="/about">about page</Link>.
                        </p>
                        <h2>How can you contact me?</h2>
                        <p>
                            You can contact me either through the <Link to="/emailme">Email form</Link> built in on this site,
                            or through <a href="https://linkedin.com/in/martin-sander-369baa1a4/" target="_blank" rel="noopener noreferrer">
                                LinkedIn</a>. You can also check out me
                            on <a href="https://stackoverflow.com/users/10146295/saddex" target="_blank" rel="noopener noreferrer">StackOverflow</a> or
                            my <a href="https://bitbucket.org/SaddexProductions/" target="_blank" rel="noopener noreferrer">Bitbucket
                            repositories</a>.
                        </p>
                    </FlexElement>
                    <FlexElement>
                        <div style={{ display: "flex", flexDirection: "column", alignContent: "center", justifyContent: "center", height: "100%" }}>
                            {images.map(i => <Image key={i._id} src={i.src} top={i.top} _id={i._id} alt="That's me"/>)}
                        </div>
                    </FlexElement>
                </FlexContainer>
                <H1 text="Past projects" />
                <P margin={wW > 900 ? "8px 10% 13px 10%" : "8px 5% 13px 5%"}>
                    The three oldest projects listed here use JQuery and JQuery UI, while the two newest use Angular and VueJS respectively. This site on its
                    own runs on the MERN stack. An admin console for this site is also currently under construction, which uses React with Typescript.
                    Common for all projects is that they use NodeJS and Express as backend, four with REST and the newest with GraphQL. 
                    Three of the projects use MongoDB as database, while another uses MySQL. Systemus Autonomus doesn't use a database.
                </P>
                <div ref={el}>
                    {projectContent}
                    {projectStatus}
                </div>
            </Wrapper>
        </Fragment>
    )
};

const mapStateToProps = state => {
    return {
        end: state.projects.end,
        projects: state.projects.projects,
        loading: state.projects.loading,
        error: state.projects.error,
        isModalOpen: state.globalUI.modalOpen,
        wW: state.globalUI.windowWidth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchProjects: (length) => { dispatch(actions.initProjects(length)) },
        modalOpen: () => { dispatch(actions.modalOpen()) },
        modalClose: () => { dispatch(actions.modalClose())},
        resetError: () => {dispatch(actions.resetProjectsError())}
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));