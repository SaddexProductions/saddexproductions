import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle, faCheckCircle } from '@fortawesome/free-solid-svg-icons';

const Text = styled.h1`
    text-align: center;
    font-weight: bold;
    color: #f00;
    font-size: 40px;
    margin: 40px 10%;

    &.warning {
        color: #ff0;
    }

    &.success {
        color: #0d0;
    }

    @media all and (max-width: 900px){
        margin: 40px 5%;
    }

    @media all and (max-width: 600px){
        font-size: 30px;
    }
`;

const warning = props => (
    <Text className={props.type} style={props.custom}>
        <FontAwesomeIcon icon={props.type === "success" ? faCheckCircle : faExclamationTriangle}/>
        <span> </span>{props.children}
    </Text>
);

export default warning;