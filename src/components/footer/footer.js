import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import ExSite from './exSite/exSite';

const FooterDiv = styled.div`
    display: block;
    background-color: #272727;
    border-bottom: none;
    height: 320px;
    margin: auto;

    .myButton {
        color: #000;
        background-color: #fff;
        position: relative;
        cursor: pointer;
        box-sizing: border-box;
        outline: 0;
        padding: 18px 1%;
        width: 240px;
        margin-top: 24px;
        font-size: 25px;
        font-weight: bold;
        border-radius: 5px;
        right: 10%;
        float: right;
        text-align: center;
        text-decoration: none;
        transition: all 0.4s ease;
    }

    .myButton:hover, .myButton:focus {
        color: #000;
        background-color: #ffd400;
        outline: 0;
    }

    .socialMedia {
        display: inline-block;
        width: 300px;
        margin-left: 8%;
    }

    @media all and (max-width: 900px){
        width: 100%;

        .socialMedia {
            display: flex;
            width: 265px;
            margin: auto;
            position: relative;
            top: 10px;
            justify-content: space-between;
        }
        .myButton {
            padding: 20px 0;
            margin: 40px auto 5px auto;
            float: none;
            right: auto;
            width: 260px;
        }
    }
`;

const FooterList = styled.div`
    display: block;
    width: 100%;
    float: left;
    margin-left: 7%;
    margin-top: 15px;

    p, a {
        font-size: 19px;
        font-weight: 500;
        margin-top: 1px;
        margin-bottom: 0;
        margin-left: calc(1.5% + 15px);
    }

    @media all and (max-width: 900px){
        text-align: center;
        margin-left: 0;

        p, a {
            margin-left: 0;
        }
    }
`;

const linkItems = [
    {
        title: "Bitbucket",
        width: "77px",
        href: "bitbucket.com/SaddexProductions",
        img: "/img/bitbucket.png"
    },
    {
        title: "Vimeo",
        width: "69px",
        href: "vimeo.com/saddex",
        img: "/img/vimeo-logo.png"
    },
    {
        title: "LinkedIn",
        width: "70px",
        href: "linkedin.com/in/martin-sander-369baa1a4/",
        img: "/img/linkedin.png"
    }
];

const footer = props => (
    <FooterDiv>
        <div className="socialMedia">
        {linkItems.map(linkI => {
            return <ExSite
                key={linkI.title}
                title={linkI.title}
                width={linkI.width}
                image={linkI.img}
                link={`https://www.${linkI.href}`}
            />
        })}
        </div>
        <Link to="/contact" className="myButton" style={{display: props.noMail ? "none" : "block"}}>
            Email me
            </Link>
        <FooterList>
            <p>Saddex Productions.</p>
            <p>All rights reserved.</p>
        </FooterList>
    </FooterDiv>
);

export default footer;