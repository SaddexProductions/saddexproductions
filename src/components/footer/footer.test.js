import React from 'react';
import ExSite from './exSite/exSite';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Footer from './footer';

configure({adapter: new Adapter()});

describe('<Footer/>',()=>{
    it('should render 3 ext link elements',()=>{
        const wrapper = shallow(<Footer/>);
        expect(wrapper.find(ExSite)).toHaveLength(3)
    })
});