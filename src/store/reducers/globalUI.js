import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../../shared/utility';

const initialState = {
    menuOpen: false,
    modalOpen: false,
    langMenu: false,
    windowWidth: window.innerWidth,
    timeStamp: null
}

const toggleMenu = state => {
    let langMenuOpen = state.langMenu;

    if(state.menuOpen) langMenuOpen = false;

    return updateObject(state,{
        menuOpen: !state.menuOpen,
        langMenu: langMenuOpen
    });
}

const closeMenu = state => {
    return updateObject(state, {
        menuOpen: false
    });
}

const setSize = state => {
    return updateObject(state,{
        windowWidth: window.innerWidth
    });
}

const openModal = state => {
    return updateObject(state,{
        modalOpen: true
    });
}

const closeModal = state => {
    return updateObject(state,{
        modalOpen: false
    });
}

const toggleLangMenu = state => {
    return updateObject(state,{
        langMenu: !state.langMenu
    });
}

const closeLangMenu = state => {
    return updateObject(state,{
        langMenu: false
    });
}

const reducer = (state = initialState,action) =>{
    switch(action.type){
        case(actionTypes.TOGGLE_MENU): return toggleMenu(state);
        case(actionTypes.CLOSE_MENU): return closeMenu(state);
        case(actionTypes.TOGGLE_LANGUAGE_MENU): return toggleLangMenu(state);
        case(actionTypes.CLOSE_LANGUAGE_MENU): return closeLangMenu(state);
        case(actionTypes.SET_SIZE): return setSize(state);
        case(actionTypes.MODAL_OPEN): return openModal(state);
        case(actionTypes.MODAL_CLOSED): return closeModal(state);
        default: return state;
    }
}

export default reducer;