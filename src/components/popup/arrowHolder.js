import React from 'react';

const arrowSideHolder = props => (
    <div style={{ display: (props.iWTS && props.wW <= 900) || (props.wW <= 900 && props.type === "video") ? "none" : "inline-table" }}>
        {props.children}
    </div>
);

export default arrowSideHolder;