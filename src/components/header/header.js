import React, { Fragment } from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import Logo from '../logo/logo';
import Menu from './menu/menu';
import Nav from '../UI/navbar/navButton/navButton';
import NavMenu from '../UI/navbar/navMenu/navMenu';
import LangMenu from './langMenu/langMenu';

const Header = styled.div`
    background-color: #000;
    height: 75px;
    border-bottom: 3px solid #fff;
    display: block;
    position:fixed;
    width: 100%;
    z-index: 52;

    .innerHeader {
        margin-top: 10px;
        width: calc(64% - 12px);
        margin: auto auto auto 18%;
        display: flex;
        justify-content: space-between;
        box-sizing: border-box;
    }

    @media all and (max-width: 900px){
        .innerHeader {
            width: 100%;
            margin: 0;
            display: block;
        }
    }
`;

const LogoContainer = styled.div`
    margin-top: 10px;
    margin-right: 25px;
    display: inline-block;
    transition: filter 0.4s ease;

    :hover {
        filter: drop-shadow(0 0 5px #fff);
    }

    a {
        outline: none
    }

    @media all and (max-width: 1100px){
        margin-right: 12px
    }

    @media all and (max-width: 900px){
        margin: 10px 20px;
    }
`;

const HeaderComp = props => {

    return (
        <Fragment>
            <Header>
                <div className="innerHeader">
                    <LogoContainer>
                        <NavLink to="/"><Logo width={1} /></NavLink>
                    </LogoContainer>
                    <Nav clicked={props.toggleMenu} />
                    <div>
                        <Menu />
                        <LangMenu {...props} />
                    </div>
                </div>
            </Header>
            <NavMenu active={props.menuOpen} clicked={props.closeMenu} />
        </Fragment>
    )
};

const mapStateToProps = state => {
    return {
        menuOpen: state.globalUI.menuOpen,
        langMenu: state.globalUI.langMenu
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setSize: () => dispatch(actions.setSize()),
        toggleMenu: () => dispatch(actions.toggleMenu()),
        closeMenu: () => dispatch(actions.closeMenu()),
        toggleLangMenu: () => dispatch(actions.toggleLangMenu()),
        closeLangMenu: () => dispatch(actions.closeLangMenu())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderComp);
