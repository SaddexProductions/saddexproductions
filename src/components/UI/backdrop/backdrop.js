import React from 'react';
import styled from 'styled-components';

const Back = styled.div`
    width: 100%;
    overflow: hidden;
    background-color: rgba(0,0,0,0.7);
    justify-content: center;
    align-items: center;
    position: fixed;
    z-index: 500;
`;

const backdrop = props => (
    <Back style={{display: props.enabled ? "flex" : "none", height: window.innerHeight}}>
        {props.children}
    </Back>
);

export default backdrop;