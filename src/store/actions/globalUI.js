import * as actionTypes from './actionTypes';

export const toggleMenu = () => {
    return {
        type: actionTypes.TOGGLE_MENU
    }
}

export const closeMenu = () => {
    return {
        type: actionTypes.CLOSE_MENU
    }
}

export const toggleLangMenu = () => {
    return {
        type: actionTypes.TOGGLE_LANGUAGE_MENU
    }
}

export const closeLangMenu = () => {
    return {
        type: actionTypes.CLOSE_LANGUAGE_MENU
    }
}

export const setSize = () => {
    return {
        type: actionTypes.SET_SIZE
    }
}

export const modalOpen = () => {
    return {
        type: actionTypes.MODAL_OPEN
    }
}

export const modalClose = () => {
    return {
        type: actionTypes.MODAL_CLOSED
    }
}