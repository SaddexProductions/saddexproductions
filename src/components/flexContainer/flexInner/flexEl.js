import React from 'react';
import styled from 'styled-components';

const El = styled.div`
    display: inline-block;
    width: 47%;

    h2 {
        margin: 25px auto 14px auto;
    }

    p {
        margin-top: 5px;
        font-size: 18px
    }

    @media all and (max-width:900px){
        width: 100%;
        p {
            font-size: 15px
        }
    }
`;

const ele = props => {

    return (
    <El>
        {props.children}
    </El>
)
};

export default ele;