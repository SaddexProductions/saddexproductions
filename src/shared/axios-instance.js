import axios from 'axios';

const instance = axios.create({
    baseURL: (process.env.NODE_ENV === "development" ? "http://localhost:5200" : "") + "/api"
});

instance.CancelToken = axios.CancelToken;
instance.isCancel = axios.isCancel;

export default instance;