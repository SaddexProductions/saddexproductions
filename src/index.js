import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import './index.css';
import App from './App';
import {createStore,applyMiddleware,compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import * as serviceWorker from './serviceWorker';

import {watchVideos,watchProjects, watchSoftware, watchEmailForm} from './store/sagas/index';

import globalUI from './store/reducers/globalUI';
import videos from './store/reducers/videos';
import projects from './store/reducers/projects';
import software from './store/reducers/software';
import form from './store/reducers/emailform';

const rootReducer = combineReducers({
    globalUI: globalUI,
    videos: videos,
    projects: projects,
    software: software,
    form: form
});

const sagaMiddleWare = createSagaMiddleware();

let composeEnhancers = null;
if (process.env.NODE_ENV === 'development') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
} else {
    composeEnhancers = compose;
}

const store = createStore(rootReducer,composeEnhancers(
    applyMiddleware(sagaMiddleWare)
));

sagaMiddleWare.run(watchVideos);
sagaMiddleWare.run(watchProjects);
sagaMiddleWare.run(watchSoftware);
sagaMiddleWare.run(watchEmailForm);

ReactDOM.render(<Provider store={store}><BrowserRouter><App /></BrowserRouter></Provider>, document.getElementById('root'));

serviceWorker.register();
