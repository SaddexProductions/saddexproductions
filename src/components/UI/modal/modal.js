import React from 'react';
import styled from 'styled-components';

import BackDrop from '../backdrop/backdrop';

const Mod = styled.div`
    background-color: #000;
    height: 300px;
    width: 50%;
    max-width: 700px;
    border: 2px solid #ffd400;
    box-sizing: border-box;
    z-index: 8000;

    @media all and (max-width: 900px){
        width: 90%;
    }
    @media all and (max-width: 600px){
        width: 100%;
    }
`;

const modal = props => (
    <BackDrop enabled={props.enabled}>
        <Mod>
            {props.children}
        </Mod>
    </BackDrop>
);

export default modal;