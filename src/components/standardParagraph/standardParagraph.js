import React from 'react';
import styled from 'styled-components';

const P = styled.p`
    font-size: 18px;

    @media all and (max-width: 900px){
        font-size: 15px;
    }
`;

const paragraph = props => (
    <P style={{margin: props.margin}}>
        {props.children}
    </P>
);

export default paragraph;