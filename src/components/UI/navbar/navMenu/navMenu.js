import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../store/actions/index';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {NavLink} from 'react-router-dom';

//assets

import uk from '../../../../assets/uk.png';

//styled components

const MenuContainer = styled.div`
    transform: translateY(0);
    transition: all 0.6s ease;
    position: fixed;
    background-color: #fff;
    z-index: 50;
    width: 100%;
    overflow-y: auto;
    display: none;
    margin-top: 55px;

    .main {
        list-style: none;
        display: inline;
    }

    li {
        width: 100%;
        overflow-x: hidden;
    }

    a, p {
        text-decoration: none;
        font-size: 22px;
        padding: 5px 10px;
        display: block;
        font-weight: bold;
        color: #000;
        width: 100%;
        box-sizing: border-box;
        margin: 0;
        cursor: pointer;
    }

    .miniFlag {
        height: 30px;
        float: right;
    }

    a:hover, a:focus, a.active, p:hover, p:focus {
        background-color: #666;
        outline: 0;
        color: #fff;
    }

    @media all and (max-width: 900px){
        display: inline-table;
        transform: translateY(-110%);

        &.active {
            transform: translateY(0);
        }
    }
`;

const navMenu = props => (
    <MenuContainer className={props.active && "active"} onClick={props.clicked}>
        <ul className="main">
            <li>
                <NavLink to="/" exact>Front page</NavLink>
            </li>
            <li>
                <NavLink to="/about">About</NavLink>
            </li>
            <li>
                <NavLink to="/videos">Videos</NavLink>
            </li>
            <li>
                <NavLink to="/contact">Contact</NavLink>
            </li>
            <li onClick={(e) => {e.stopPropagation(); props.toggleLangMenu()}}>
                <p>
                    <FontAwesomeIcon 
                    icon={faChevronRight} 
                    style={{transition: "transform 0.4s ease", transform: props.langMenu ? "rotate(90deg)" : "none", marginRight: "10px"}}/>
                     Language
                     <img src={uk} className="miniFlag" alt="miniflag"/>
                    </p>
            </li>
            <li style={{display: props.langMenu ? "inline" : "none"}}>
                <ul>
                    <li><p>Coming soon</p></li>
                </ul>
            </li>
        </ul>
    </MenuContainer>
);

const mapStateToProps = state => {
    return {
        langMenu: state.globalUI.langMenu
    }
}

const mapDispatchToProps = dispatch => {
    return {
        toggleLangMenu: () => {dispatch(actions.toggleLangMenu())}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(navMenu);