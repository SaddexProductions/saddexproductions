import React from 'react';
import styled from 'styled-components';
const Input = styled.div`
    width:100%;
    padding: 0;
    margin-top: 15px;
    box-sizing:border-box;
    input,textarea,select {
        outline:none;
        border:1px solid #ccc;
        background-color:white;
        font:inherit;
        padding:6px 10px;
        display:block;
        width: calc(100% - 22px);
    }
    input:focus, textarea:focus, select:focus {
        outline:none;
        background-color:#ccc;
    }
    .invalid {
        border: 1px solid #f00;
        background-color: #FDA49A;
    }
    `;

const input = (props) => {

    let inputElement = null;
    let inputClasses = null;

    if(props.invalid && props.shouldValidate && props.touched){
        inputClasses = "invalid";
    }

    switch (props.elementType) {
        case ('input'):
            inputElement = <input {...props.elementConfig} value={props.value} onChange={props.changed} className={inputClasses} aria-label={props.name}/>
            break;
        case ('textarea'):
            inputElement = <textarea {...props.elementConfig} 
            value={props.value} 
            aria-label={props.name}
            onChange={props.changed} 
            className={inputClasses} 
            onKeyUp={props.keyup}/>
            break;
        case ('select'):
        inputElement = (<select 
        {...props.elementConfig} 
        value={props.value} onChange={props.changed}
        aria-label={props.name}
        className={inputClasses}>
        {props.elementConfig.options.map(option =>(
            <option key={option.value} value={option.value}>{option.displayValue}</option>
        ))}
        </select>);
        break;
        default:
            inputElement = <input {...props.elementConfig} value={props.value} onChange={props.changed}/>
    }
    return (
        <Input key="container">
            {inputElement}
        </Input>
    );
}

export default input;