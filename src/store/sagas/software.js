import { put } from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../shared/axios-instance';

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

export function* initSoftwareSaga() {
    yield put(actions.initSoftwareStart(source));
    try {
        const res = yield axios.get('/software',
        {
            cancelToken: source.token
        });
        yield put(actions.initSoftwareSuccess(res.data));
    }
    catch (err) {
        yield put(actions.initSoftwareFail(err,axios.isCancel));
    };
}