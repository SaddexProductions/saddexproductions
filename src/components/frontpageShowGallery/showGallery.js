import React, { useState } from 'react';
import styled from 'styled-components';

const Gallery = styled.div`
    width: 80%;
    background-color: #bbb;
    border: 3px solid #ffd400;
    box-sizing: border-box;
    margin: 25px auto;
    height: 300px;
    position: relative;

    .imgHolder {
        width: 98%;
        height: 280px;
        margin: 10px auto;
        display: block;
        box-sizing: border-box;
        background-attachment: fixed;
        background-repeat: no-repeat;
        background-position-x: center;
        background-size: cover;
        filter: brightness(20%) blur(4px);
        cursor: pointer;
        transition: filter 0.4s ease;
        z-index: 3;
    }

    .imgHolder:hover, .imgHover:focus {
        filter: brightness(70%) blur(0px)
    }

    h2 {
        z-index: 5;
        position: absolute;
        margin-left: 20px;
    }

    .textContainer {
        position: absolute;
        z-index: 4;
        display:block;
        width: 100%;
        margin-top: 40px;
        pointer-events:none;
        transition: filter 0.4s ease;
        filter: opacity(0);
    }

    .textContainer.active {
        filter: opacity(1);
    }

    .textContainer p {
        text-align: left;
        display: block;
        position: relative;
        margin-top: 20px;
        margin-bottom: 25px;
        margin-left: 33%;
        width: 50%;
    }

    @media all and (max-width: 900px){
        .imgHolder {
            background-attachment: inherit;
            filter: brightness(20%) blur(2px);
        }

        .textContainer {
            margin-top: 60px;
        }
        .textContainer p {
            margin-top: 10px;
            margin-bottom: 1px;
            margin-left: 33%;
            width: 70%;
            margin-left: 16%;
        }
    }
`;

const GalleryObj = props => {

    const [showText, setShowText] = useState(true);

    return (
        <Gallery>
            <h2>{props.title}</h2>
            <div className={showText ? "textContainer active" : "textContainer"}>
                <p><strong>Type:</strong> {props.type}</p>
                <p><strong>Front-End:</strong> {props.front}</p>
                <p><strong>Back-End:</strong> {props.back}</p>
                <p><strong>Date of completion:</strong> {props.dOC}</p>
            </div>
            <a href={`https://${props.url}`} target="_blank" rel="noopener noreferrer">
            <div className="imgHolder" onMouseEnter={() => setShowText(false)} onMouseOut={() => setShowText(true)}
                style={{ backgroundImage: `url(${props.image.toString()})`, 
                backgroundPositionY: window.innerWidth > 900 ? (150 - props.offset).toString() + "px" : "center"}} />
            </a>
        </Gallery>
    )
};

export default GalleryObj;