import React from 'react';
import Modal from './modal';
import ErrorText from './errorText/errorText';
import ModalButtonHolder from './errorModalButtonHolder/errorModalButtonHolder';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

const modal = props => (
    <Modal enabled={props.enabled}>
        <ErrorText>
            <FontAwesomeIcon icon={faExclamationTriangle}/>
            {" " + props.errorMsg}
        </ErrorText>
        <p style={{textAlign:"center", fontWeight:"bold"}}>{props.details}</p>
        <ModalButtonHolder err={props.error}>
            {props.children}
        </ModalButtonHolder>
    </Modal>
);

export default modal;
