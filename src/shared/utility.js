export const updateObject = (oldObject, updatedProperties) =>{
    return {
        ...oldObject,
        ...updatedProperties
    }
}

export const checkValid = (val,rules) =>{
    let isValid = true;

    if(rules.required){
        isValid = val.trim() !== "" && isValid;
    }
    if(rules.minLength){
        isValid = val.length >= rules.minLength && isValid;
    }
    if(rules.maxLength){
        isValid = val.length <= rules.maxLength && isValid;
    }

    if(rules.isEmail){
        const pattern =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        isValid = pattern.test(val) && isValid;
    }
    if(rules.isNumeric) {
        const pattern = /^\d+$/;
        isValid = pattern.test(val) && isValid;
    }
    return isValid;
}

export const months = {
    english: ["January","February","March","April","May","June","July","August","September","October","November","December"]
}

export const checkEmpty = (obj,allEmpty) => {

    for (let key in obj) {
        if (obj[key].value !== "") allEmpty = false;
    }

    return allEmpty;
}

export const isInViewport = (yourElement, offset = 0) => {
    if (!yourElement) return false;
    const top = yourElement.getBoundingClientRect().top;
    return (top + offset) >= 0 && (top - offset) <= window.innerHeight;
}

export const convertError = err => {
    if (err.response && err.response.data && err.response.data.indexOf("html") === -1) {
        return err.response.data;
    }
    else {
        return err.message;
    }
}