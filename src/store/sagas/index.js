import { takeLatest, takeEvery } from 'redux-saga/effects';

import {initVideosSaga} from './videos';
import {initProjectsSaga} from './projects';
import {initSoftwareSaga} from './software';
import {sendMailSaga} from './emailform';
import * as actionTypes from '../actions/actionTypes';

export function* watchVideos(){
    yield takeLatest(actionTypes.INIT_VIDEOS, initVideosSaga);
}

export function* watchEmailForm(){
    yield takeEvery(actionTypes.INIT_FORM_SEND, sendMailSaga);
}

export function* watchProjects(){
    yield takeLatest(actionTypes.INIT_PROJECTS, initProjectsSaga);
}

export function* watchSoftware(){
    yield takeLatest(actionTypes.INIT_SOFTWARE, initSoftwareSaga);
}