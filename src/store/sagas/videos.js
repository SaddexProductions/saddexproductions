import { put } from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../shared/axios-instance';

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

export function* initVideosSaga(action) {
    yield put(actions.initVideosStart(source));
    try {
        let query = "";
        if(action.filter){
            let obj = {...action.filter};
            obj.desc = obj.desc.trim();
            obj.name = obj.name.trim();
            for (let propName in obj) { 
                if (obj[propName] === undefined || obj[propName] === null || obj[propName] === "") {
                  delete obj[propName];
                }
            }
            query = "?" + Object.keys(obj).map(key => key + '=' + obj[key]).join('&');
        }
        const res = yield axios.get('/videos' + query,
        {
            cancelToken: source.token
        });
        yield put(actions.initVideosSuccess(res.data));
    }
    catch (err) {
        yield put(actions.initVideosFail(err, axios.isCancel));
    };
}