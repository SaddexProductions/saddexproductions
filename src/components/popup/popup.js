import React, { useState, useEffect, Fragment, useRef } from 'react';
import styled from 'styled-components';
import queryString from 'query-string';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleLeft, faArrowCircleRight, faTimes } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';
import Parser from 'html-react-parser';

//components
import Backdrop from '../UI/backdrop/backdrop';
import Modal from '../UI/modal/modalPackage';
import Button from '../UI/button/button';
import ArrowSideHolder from './arrowHolder';

//styledComponents

const PopupInner = styled.div`
    width: 100%;
    display: flex;
    overflow-x: hidden;
    justify-content: center;
    align-content: center;
    position: fixed;
    z-index: 550;
    height: 100%;
    
    &.multipleImages {
        justify-content: space-between;
    }

    svg {
        margin-top: calc(50vh - 20px);
        font-size: 40px;
        color: #fff;
        cursor: pointer;
        transition: transform 0.4s ease;
    }

    svg:hover, svg:focus {
        transform: scale(1.1);
    }

    &.multipleImages .left {
        margin-left: 20px;
    }

    &.multipleImages .right {
        margin-right: 20px;
    }

    iframe {
        margin-top: 80px;
        width: calc(90% - 50px);
        margin-bottom: 120px;
        background-color: #000;
    }

    .imgHolder {
        height: calc(100vh - 100px);
        margin-bottom: 5px;
        overflow: hidden;
    }

    .imgHolder img {
        height: 100%;
        display: block;
        margin: auto;
    }

    .imgHolder.wide {
        width: calc(100% - 140px);
        height: auto;
        overflow: hidden
    }

    .imgHolder.wide img {
        height: auto;
        width: 100%;
    }

    .imgHolder p {
        font-size: 12px;
        padding: 1px;
        box-sizing: border-box;
        width: calc(100% + 1px);
        z-index: 5100;
        background-color: rgba(0,0,0,0.8);
        bottom: 0;
        transform: translateY(-28px);
        overflow-x: auto;
    }

    .desc {
        position: absolute;
        background-color: #222;
        bottom: 0;
        height: 120px;
        overflow-y: auto;
    }

    .desc p, h3 {
        margin: 10px 10px;
    }

    @media all and (max-width:900px){
        .desc {
            width: 100% !important;
        }
        iframe {
            width: 100%;
            background: none;
        }

        .overLayArrows svg {
            color: rgba(255,255,255,0.3);
            transition: color .4s ease, transform .4s ease;
        }
        .overLayArrows svg:hover, .overLayArrows svg:focus {
            color: #fff;
        }

        .imgHolder.wide {
            width: 100%;
        }

        .imgHolder p {
            bottom: 0;
        }
    }
`;

const CrossHolder = styled.div`
    .cross {
        color: #fff;
        position:absolute;
        top: 20px;
        display: inline-block;
        right: 20px;
        z-index: 5000;
        font-size: 30px;
        cursor: pointer;
        transition: transform 0.4s ease;
    }

    .cross:hover, .cross:focus {
        transform: scale(1.1);
    }

    iframe {
        background-color: none
    }
`;

const Popup = props => {

    //variables, constants, state

    let objToRender,
        index = 1,
        bulkContent,
        arrowLeft,
        arrowRight;

    const params = queryString.parse(props.location.search);

    const [multipleItems, setMultipleItems] = useState(null);

    const [popupError, setPopupError] = useState(null);

    const [imageWiderThanScreen, setIWTS] = useState(false);

    const [videoWidth, setVideoWidth] = useState(null);

    const [imgMargin, setMargin] = useState(null);

    const el = useRef(null);

    const { wW, type } = props;

    //methods

    const browse = dir => {
        if (((index > 0) && (index < props.items.length - 1)) || ((index === 0) && (dir > 0)) || ((index === props.items.length - 1) && (dir < 0))) {
            index += dir;
        }
        else if (index === 0 && dir < 0) {
            index = props.items.length - 1;
        }
        else {
            index = 0;
        }
        props.history.push("popup?itemID=" + props.items[index]._id);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const close = () => {
        props.history.push("./");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const setPos = () => {
        if (el && el.current) {
            if (type === "video") {
                setVideoWidth(el.current.clientWidth);
            }
            else {
                setMargin(window.innerHeight * 0.5 - ((el.current.height / 2)) - 5);
                if (((window.innerHeight) / (window.innerWidth)) > 1 * ((el.current.height / el.current.width) * 1.03)) {
                    setIWTS(true)
                }
                else {
                    setIWTS(false);
                }
            }
        }
    }

    //useEffect calls

    useEffect(() => {
        setMultipleItems(props.items.length > 1);

        if((!Array.isArray(props.items) && props.error) || (props.items.length === 0 && props.error)){
            close()
        }

    }, [props.items, props.error, close]);

    useEffect(() => {
        setPos();
    }, [type, el, setPos]);

    //inline-styles

    const arrowStyle = {
        display: multipleItems ? "inherit" : "none",
        pointerEvents: "auto"
    }

    //modals

    if (popupError) {
        bulkContent = (
            <Modal enabled errorMsg={popupError.msg} error={popupError} details="The url is likely faulty!">
                <Button clicked={() => close()}>Okay</Button>
            </Modal>
        );
    }

    arrowLeft = (
        <FontAwesomeIcon
            className="arrow left"
            icon={faArrowCircleLeft}
            style={arrowStyle}
            title="Previous item"
            onClick={(e) => {
                e.stopPropagation();
                browse(-1);
            }}
        />
    );

    arrowRight = (

        <FontAwesomeIcon
            className="arrow right"
            icon={faArrowCircleRight}
            title="Next item"
            style={arrowStyle}
            onClick={(e) => {
                e.stopPropagation();
                browse(1);
            }}
        />
    );

    if (props.items && props.items.length > 0 && !popupError) {
        try {
            index = (props.items.findIndex(i => i._id === params.itemID));

            if (props.items.length > 0 && !props.items.find(x => x._id === params.itemID)) {
                throw { msg: "Item not found!", userError: true };
            }

            if (props.type === "video") {
                objToRender = (
                    <Fragment>
                        <iframe
                            ref={el}
                            title="vimeo-player"
                            src={props.items[index].adress}
                            onLoad={setPos}
                            frameBorder="0"
                            allowFullScreen />
                        <div className="desc" onClick={(e) => e.stopPropagation()}
                            style={{ width: videoWidth + "px" }}
                        >
                            <h3>{props.items[index].uploaded.split('T')[0]}</h3>
                            <div style={{ margin: "10px" }}>{Parser(props.items[index].desc)}</div>
                        </div>
                    </Fragment>
                );

            }

            else {
                objToRender = (
                    <div
                        className={imageWiderThanScreen ? "imgHolder wide" : "imgHolder"}
                        onClick={e => e.stopPropagation()}
                        style={{ marginTop: imgMargin }}
                    >
                        <img
                            src={props.items[index].src}
                            alt={props.items[index]._id}
                            loading="lazy"
                            ref={el}
                            onLoad={setPos}
                        />
                        <p style={{ display: props.items[index].caption ? "inline-table" : "none" }}>{props.items[index].caption}</p>
                    </div>
                );
            }
            bulkContent = (
                <Fragment>
                    <Helmet>
                        <title>
                            {(props.items[index].name ? props.items[index].name : props.items[index]._id) + " - Saddex Productions"}
                        </title>
                    </Helmet>
                    <Backdrop enabled />
                    <PopupInner
                        style={{
                            alignContent: imageWiderThanScreen || props.type === "video" ? "center" : "space-between",
                            justifyContent: imageWiderThanScreen || props.type === "video" ? "center" : "space-between"
                        }}
                        onClick={() => {if(props.type !== "video") close()}}
                        className={(props.type === "image" && props.items.length > 1) && "multipleImages"}
                    >
                        <ArrowSideHolder iWTS={imageWiderThanScreen} wW={wW} type={props.type}>
                            {arrowLeft}
                        </ArrowSideHolder>
                        {objToRender}
                        <ArrowSideHolder iWTS={imageWiderThanScreen} wW={wW} type={props.type}>
                            {arrowRight}
                        </ArrowSideHolder>
                        <div style={{ 
                            position: "absolute", 
                            width: "100%",
                            pointerEvents: "none",
                            display: (imageWiderThanScreen && wW > 900) || (props.type ==="video" && wW > 900) ? "none" : "block" 
                            }}
                            className="overLayArrowsContainer">
                            <div style={{
                                display: "flex",
                                width: "100%",
                                justifyContent: "space-between",
                                alignContent: "space-between",
                                pointerEvents: "none"
                            }}
                            className="overLayArrows"
                            >
                                {arrowLeft}
                                {arrowRight}
                            </div>
                        </div>
                    </PopupInner>
                    <CrossHolder>
                        <FontAwesomeIcon icon={faTimes}
                            className="cross"
                            title="Close"
                            onClick={close}
                        />
                    </CrossHolder>
                </Fragment>
            );
        }
        catch (err) {
            setPopupError(err);
        }
    }

    return (
        <Fragment>{bulkContent}</Fragment>
    );
}

const mapStateToProps = state => {
    return {
        wW: state.globalUI.windowWidth
    }
}

export const PopupTest = withRouter(Popup);

export default withRouter(connect(mapStateToProps)(Popup));