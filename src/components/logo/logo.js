import React from 'react';

const logo = props => (
    <img src="/img/saddexprods.png" style={{width: (110*props.width).toString() + "px"}} alt="Logo"/>
);

export default logo;