import React from 'react';
import styled from 'styled-components';

const Flex = styled.div`
    width: 80%;
    margin: auto;
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media all and (max-width:900px){
        display: block;
        width: 90%;
    }
`;

const flex = props => (
    <Flex>
        {props.children}
    </Flex>
);

export default flex;