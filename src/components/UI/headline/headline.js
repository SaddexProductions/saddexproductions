import React from 'react';
import styled from 'styled-components';

const HeadLine = styled.h1`
    font-size: 40px;
    color: #fff;
    padding: 0 10%;
    width: 60%;
    display: inline-block;
    margin: 35px 0 7px 0;

    @media all and (max-width: 900px){
        font-size: 30px;
        margin: 18px 0 7px 0;
        width: 90%;
        padding: 0 5%;
    }
`;

const headLine = props => (
    <HeadLine>
        {props.text}
    </HeadLine>
);

export default headLine;