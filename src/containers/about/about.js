import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import {withRouter,Switch,Route,Link} from 'react-router-dom';

//components
import H1 from '../../components/UI/headline/headline';
import Wrapper from '../../components/UI/wrapper/wrapper';
import Image from '../../components/UI/img/img';
import FlexContainer from '../../components/flexContainer/flexContainer';
import FlexElement from '../../components/flexContainer/flexInner/flexEl';
import Popup from '../../components/popup/popup';

const aboutImages = [
    {
        src: "/img/vp.jpg",
        _id: "vp.png",
        caption: "Vilken Politik, a game built with JQuery/JQuery UI, NodeJS/Express and MySQL."
    },
    {
        src: "/img/rig.png",
        _id: "rig.png",
        caption: "A rig of a Fox done in Maya that I did, doing 3D."
    }
];

const about = props => (
    <Fragment>
        <Helmet>
            <title>About - Saddex Productions</title>
        </Helmet>
        <Switch>
            <Route path="/about/popup"
            render={() => <Popup {...props} items={aboutImages} type="image"/>}
            />
        </Switch>
        <Wrapper>
            <div style={{ marginBottom: "40px" }}>
                <H1 text="About me" />
                <FlexContainer>
                    <FlexElement>
                        <p>
                            As a developer I stride to learn new technologies and challenge myself by building
                            complex applications. I can utilise all the three big front-end frameworks, and am most
                            fluent with React. As I am currently building my first big MEAN-stack application however,
                            Angular is quickly becoming one of my favorites, primarily due to its use of services and
                            how easy they make state management out of the box. I have aquired almost three years experience
                            with Javascript both through university and self-learning, but prefer Typescript these days due
                            to that it prevents most runtime errors and also makes communication between potential team members
                            simpler. Other languages I know are Python, C, Java and C#. I learned Python and Java on my own and
                            am primarily fluent in the former, partly due to learning Django, while I still need to learn how to
                            put the latter to practical use through for example Android development, which I plan to learn later
                            as soon as possible. I am as a person a bit of a perfectionist and try to keep my code clean and non-redundant
                            as much as possible, which is why I try to learn frameworks and technologies through and through.
                        </p>
                        <p>
                            My primary field of interest before going to university used to be video production,
                            special effects and 3D. Through that and self-learning,
                            I have skills in After Effects, Premiere Pro, Cinema 4D, Autodesk Maya,
                            Photoshop and Illustrator, among various plugins for a few of those programs.
                        </p>
                        <p>
                            I discovered my interest for
                            that type of software through editing and uploading videos on my Youtube channel
                            back in the day, but ultimately I changed my mind in 2017.
                            I am although still considering utilising the skills from these fields in a
                            supplementary manner. I took my diploma in June 2019 at the institution of Informatics at Umeå University, but had then
                            entirely changed my direction towards web/software development which also was taught there. I have collected
                            much of my past work on the <Link to="/videos">videos' page</Link>, which
                            consists of a mix of personal experiments, university projects and amateur
                            freelance work I got paid for to do.
                        </p>
                        <p>
                            In my freetime I like to go on bike trips, hiking, skiing and play video games. I also go to
                            the gym in periods. I went to Germany short after recieving my diploma 2019 as I wanted a new environment
                            to be in, and spent the time improving my German from July 2019-December 2019 when I reached certified C1-level fluency.
                            During 2020, and especially during the Coronavirus lockdown, I have spent large amounts of time learning new
                            technologies and building projects. I returned to Sweden in December 2020.
                        </p>
                    </FlexElement>
                    <FlexElement>
                        {aboutImages.map(im => <Image key={im._id} src={im.src} caption={im.caption} _id={im._id} path="/about"/>)}
                    </FlexElement>
                </FlexContainer>
                <H1 text="About this site" />
                <FlexContainer>
                    <FlexElement>
                        <p>
                            The front-end of this was built with React 16+ using only functional components. It uses React Router for navigation,
                            Redux for state management, and Axios for http requests.
                    </p>
                        <p>
                            Back-end uses NodeJS, Express and MongoDB (Mongoose driver) for now.
                    </p>
                        <p>
                            All of it runs on an Ubuntu 16.04 VPS that I have configured myself.
                    </p>
                    </FlexElement>
                    <FlexElement>

                    </FlexElement>
                </FlexContainer>
            </div>
        </Wrapper>
    </Fragment>
);

export default withRouter(about);