import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

const ImageHolder = styled.div`
    margin-bottom: 60px;

    img {
        border-radius: 10px;
        width: 80%;
        max-width: 330px;
        cursor: pointer;
        display: inline-block;
        margin-left: auto;
        float: right;
    }
    p {
        font-size: 13px !important;
        width: 80%;
        max-width: 330px;
        margin-left: auto;
    }

    @media all and (max-width:900px){
        margin-bottom: 30px;
        img {
            width: 70%;
            display: block;
            float: none;
            margin: 20px auto 0 auto;
        }

        p {
            display: block !important;
            margin-left: auto !important;
            margin-right: auto !important;
            width: 70%;
        }
    }
    @media all and (max-width:550px){
        img {
            width: 100%;
        }

        p {
            width: 100%;
        }
    }
`;

const image = props => (
    <ImageHolder style={{marginTop: props.top }}>
        <Link to={(props.path ? props.path : "")+ "/popup?itemID=" + props._id}>
        <img src={props.src} style={{ marginTop: props.top }} 
        loading="lazy" alt={props.alt ? props.alt : props.caption}
        />
        </Link>
        <p>{props.caption}</p>
    </ImageHolder>
);

export default image;