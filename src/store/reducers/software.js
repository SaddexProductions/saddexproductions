import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    loading: false,
    software: [],
    error: null,
    cancelToken: null
};

const initSoftwareStart = (state, action) => {
    return updateObject(state, {
        loading: true,
        software: [],
        error: null,
        cancelToken: action.cancelToken
    });
}

const initSoftwareSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        software: action.data,
        cancelToken: null
    });
}

const initSoftwareFail = (state, action) => {
    if (!action.isCancel(action.error)) {
        return updateObject(state, {
            loading: false,
            error: action.error,
            cancelToken: null
        });
    }
    else {
        return updateObject(state, {loading: false});
    }
}

const resetSoftwareError = state => {
    return updateObject(state, {
        error: null
    });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case (actionTypes.INIT_SOFTWARE_START): return initSoftwareStart(state, action);
        case (actionTypes.INIT_SOFTWARE_SUCCESS): return initSoftwareSuccess(state, action);
        case (actionTypes.INIT_SOFTWARE_FAIL): return initSoftwareFail(state, action);
        case (actionTypes.RESET_SOFTWARE_ERROR): return resetSoftwareError(state);
        default: return state;
    }
}

export default reducer;