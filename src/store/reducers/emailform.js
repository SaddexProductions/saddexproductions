import * as actionTypes from '../actions/actionTypes';
import { updateObject, checkValid } from '../../shared/utility';

const formContent = {
    form: {
        from: {
            elementType: "input",
            elementConfig: {
                type: "email",
                placeholder: "example@gmail.com"
            },
            value: "",
            title: "Email address",
            validation: {
                required: true,
                isEmail: true,
                minLength: 10,
                maxLength: 100
            },
            valid: false,
            touched: false
        },
        subject: {
            elementType: "input",
            elementConfig: {
                type: "text",
                placeholder: "Example inquiry"
            },
            value: "",
            title: "Subject",
            validation: {
                required: true,
                minLength: 3,
                maxLength: 120
            },
            valid: false,
            touched: false
        },
        content: {
            elementType: "textarea",
            elementConfig: {
                placeholder: "Your message here... (max 10 000 characters)"
            },
            value: "",
            title: "Message content",
            validation: {
                required: true,
                minLength: 5,
                maxLength: 10000
            },
            valid: false,
            touched: false
        },
    },
    isValid: false
}

const initialState = {
    loading: false,
    content: {
        ...formContent
    },
    error: null,
    cancelToken: null,
    modal: false,
    success: false
};

const setTouched = state => {
    let updatedForm = {...state.content.form};

    for (let key in state.content.form) {
        updatedForm[key] = updateObject(state.content.form[key], {
            touched: true,
            valid: checkValid(state.content.form[key].value, state.content.form[key].validation)
        });
    }

    return validate(state,updatedForm);
};

const updateForm = (state, action) => {
    const upFormEl = updateObject(state.content.form[action.id], {
        value: action.e.target.value,
        valid: checkValid(action.e.target.value, state.content.form[action.id].validation),
        touched: true
    });
    const updatedForm = updateObject(state.content.form, {
        [action.id]: upFormEl
    });

    updatedForm[action.id] = upFormEl;

    return validate(state,updatedForm);
}

const validate = (state,updatedForm) =>{
    let formIsValid = true;
    for (let inId in updatedForm) {
        formIsValid = updatedForm[inId].valid && formIsValid;
    }

    return updateObject(state, { content: { form: updatedForm, formIsValid: formIsValid } });
}

const sendFormStart = (state,action) => {
    return updateObject(state, {
        loading: true,
        error: null,
        success: false,
        modal: false,
        cancelToken: action.cancelToken
    });
}

const sendFormSuccess = state => {
    return updateObject(state, {
        loading: false,
        content: {
            ...formContent
        },
        success: true,
        cancelToken: null
    });
}

const sendFormFail = (state, action) => {

    if(!action.isCancel(action.error)){
        return updateObject(state, {
            loading: false,
            error: action.error,
            modal: true,
            cancelToken: null
        });
    }
    else {
        return updateObject(state, {loading: false});
    }
}

const reset = state => {
    return updateObject(state, {
        modal: false,
        success: false
    });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case (actionTypes.UPDATE_FORM): return updateForm(state, action);
        case (actionTypes.SET_TOUCHED): return setTouched(state);
        case (actionTypes.SEND_FORM_START): return sendFormStart(state,action);
        case (actionTypes.SEND_FORM_SUCCESS): return sendFormSuccess(state);
        case (actionTypes.SEND_FORM_FAIL): return sendFormFail(state, action);
        case (actionTypes.RESET): return reset(state);
        case (actionTypes.EMPTY_FORM): return initialState;
        default: return state;
    }
}

export default reducer;