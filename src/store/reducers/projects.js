import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    loading: false,
    projects: [],
    error: null,
    end: false,
    cancelToken: null
};

const initProjectsStart = (state, action) => {
    return updateObject(state, {
        loading: true,
        error: null,
        cancelToken: action.cancelToken
    });
}

const initProjectsSuccess = (state, action) => {

    return updateObject(state, {
        loading: false,
        projects: [
            ...state.projects,
            ...action.data
        ],
        cancelToken: null,
        end: action.data.length < 5
    });
}

const initProjectsFail = (state, action) => {
    if (!action.isCancel(action.error)) {
        return updateObject(state, {
            loading: false,
            error: action.error,
            cancelToken: null
        });
    }
    else {
        return updateObject(state, {loading: false});
    }
}

const resetProjectsError = state => {
    return updateObject(state, {
        error: null
    });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case (actionTypes.INIT_PROJECTS_START): return initProjectsStart(state, action);
        case (actionTypes.INIT_PROJECTS_SUCCESS): return initProjectsSuccess(state, action);
        case (actionTypes.INIT_PROJECTS_FAIL): return initProjectsFail(state, action);
        case (actionTypes.RESET_PROJECTS_ERROR): return resetProjectsError(state);
        default: return state;
    }
}

export default reducer;