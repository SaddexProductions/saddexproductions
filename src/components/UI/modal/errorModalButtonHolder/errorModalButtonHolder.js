import React from 'react';
import styled from 'styled-components';

const Holder = styled.div`
    display: flex;
    width: 420px;
    margin: 50px auto 20px auto;

    @media all and (max-width: 600px){
        width: 335px;
    }
`;

const holder = props => (
    <Holder style={{justifyContent: props.err && props.err.userError ? "center" : "space-between"}}>
        {props.children}
    </Holder>
);

export default holder;