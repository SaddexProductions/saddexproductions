import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../../shared/utility';

const initialState = {
    loading: false,
    videos: [],
    error: null,
    cancelToken: null
};

const initVideosStart = (state,action) => {
    return updateObject(state,{
        loading: true,
        videos: [],
        error: null,
        cancelToken: action.cancelToken
    });
}

const initVideosSuccess = (state,action) => {
    return updateObject(state,{
        loading: false,
        videos: action.data,
        cancelToken: null
    });
}

const initVideosFail = (state,action) => {
    if(!action.isCancel(action.error)){
        return updateObject(state,{
            loading: false,
            error: action.error,
            cancelToken: null
        });
    }
    else {
        return updateObject(state, {loading: false});
    }
    
}

const resetVideoError = state => {
    return updateObject(state,{
        error: null
    });
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case(actionTypes.INIT_VIDEOS_START): return initVideosStart(state,action);
        case(actionTypes.INIT_VIDEOS_SUCCESS): return initVideosSuccess(state,action);
        case(actionTypes.INIT_VIDEOS_FAIL): return initVideosFail(state,action);
        case(actionTypes.RESET_VIDEO_ERROR): return resetVideoError(state);
        default: return state;
    }
}

export default reducer;