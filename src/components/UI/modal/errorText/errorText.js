import React from 'react';
import styled from 'styled-components';

const Etext = styled.h1`
    color: #f00;
    font-size: 24px;
    margin: 60px auto;
    text-align: center;
    font-weight: bold;

    @media all and (max-width: 600px){
        margin: 60px auto 30px auto;
    }
`;

const textModal = props => (
    <Etext>
        {props.children}
    </Etext>
);

export default textModal;