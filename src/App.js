import React, { useEffect, useState } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import './App.css';
import { connect } from 'react-redux';
import * as actions from './store/actions/index';
import {checkEmpty} from './shared/utility';
import useUnload from './hooks/useUnload';

import Main from './containers/main/main';
import About from './containers/about/about';
import Videos from './containers/videos/videos';
import Mail from './containers/emailform/emailform';
import Header from './components/header/header';

const App = props => {

  //useState, constants

  const {setSize,closeMenu,form,closeLangMenu,langMenu} = props;
  const [windowTimeOut,setWindowTimeOut] = useState(false);

  const formEmpty = checkEmpty(form,true);

  //useEffect, hooks

  useEffect(() => {
    setSize();
    
    window.addEventListener('resize', () => {
      if(!windowTimeOut){
        setSize();
        setWindowTimeOut(true);
        if(window.innerWidth > 900){
          closeMenu();
        }
        else {
          closeLangMenu();
        }
        setTimeout(() => setWindowTimeOut(false),200);
      }
    });
  
  }, [setSize, closeMenu, windowTimeOut,form,langMenu,closeLangMenu]);

  useUnload(!formEmpty);

return (
  <div onClick={
    () => {if(langMenu) closeLangMenu()}
  }>
    <Header />
    <Switch>
      <Route path="/about" component={About} />
      <Route path="/videos" component={Videos} />
      <Route path="/contact" component={Mail} />
      <Route path="/" component={Main} />
      <Redirect to="/"/>
    </Switch>
  </div>
);
}

const mapStateToProps = state => {
  return {
    form: state.form.content.form,
    langMenu: state.globalUI.langMenu
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setSize: () => dispatch(actions.setSize()),
    closeMenu: () => dispatch(actions.closeMenu()),
    closeLangMenu: () => dispatch(actions.closeLangMenu())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
