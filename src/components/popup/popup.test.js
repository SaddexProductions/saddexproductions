import React from 'react';
import {PopupTest} from './popup';

import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

//test broken, needs to switch from Enzyme to React testing library to make it work

describe('<Popup>',()=> {

    it('should have arrows hidden when item array length is less than 2',()=>{
        const wrapper = shallow(<PopupTest items={
            [{
                src: "/img/me.png"
            },
            {
                src: "/img/me.png"
            }
        
        ]
        }/>);

        expect(wrapper.find('.overLayArrowsContainer')).toBeVisible()
    });

});