import {useEffect} from 'react';

const usePreventWindowUnload = (preventDefault) => {
    useEffect(() => {
      if (!preventDefault) return;
      const handleBeforeUnload = e => e.preventDefault();
      window.addEventListener("beforeunload", handleBeforeUnload);
      return () => window.removeEventListener("beforeunload", handleBeforeUnload);
    }, [preventDefault]);
}

export default usePreventWindowUnload;