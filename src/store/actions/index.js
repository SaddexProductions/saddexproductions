export {toggleMenu,closeMenu,setSize,modalOpen,modalClose,toggleLangMenu,closeLangMenu} from './globalUI';
export {initVideosStart,initVideosSuccess,initVideosFail, initVideos,resetVideoError} from './videos';
export {initProjects,initProjectsStart,initProjectsSuccess,initProjectsFail,resetProjectsError} from './projects';
export {initSoftware,initSoftwareStart,initSoftwareSuccess,initSoftwareFail,resetSoftwareError} from './software';
export {initFormSend,sendFormStart,sendFormSuccess,sendFormFail,reset,updateForm,setTouched,emptyForm} from './emailform';