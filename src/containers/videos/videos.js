import React, { Fragment, useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import styled from 'styled-components';
import { Route, Link, Switch } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';
import { isInViewport, convertError } from '../../shared/utility';

//components
import H1 from '../../components/UI/headline/headline';
import Wrapper from '../../components/UI/wrapper/wrapper';
import Spinner from '../../components/UI/spinner/spinner';
import Modal from '../../components/UI/modal/modalPackage';
import Button from '../../components/UI/button/button';
import BodyErrorText from '../../components/UI/bodyErrorText/bodyErrorText';
import P from '../../components/standardParagraph/standardParagraph';
import FlexContainer from '../../components/flexContainer/flexContainer';
import FlexInner from '../../components/flexContainer/flexInner/flexEl';
import Popup from '../../components/popup/popup';

//styled components

const Article = styled.article`
    width: 220px;
    display: inline-table;
    margin-left: 20px;
    margin-right: 15px;
    box-sizing: border-box;

    img { 
        border: 1px solid #fff;
        border-radius: 2px;
        width: 100%;
        cursor: pointer;
        transition: filter 0.4s ease;
    }

    img:hover,img:focus {
        filter: brightness(124%);
    }

    h3 {
        margin-top: 2px;
        margin-bottom: 3px;
        text-align: center;
    }

    p {
        font-size: 15px;
        margin-top: 5px;
    }
    @media all and (max-width: 600px){
        position: relative;
        display: inline-block;
        margin: auto;
        width: 90%;
    }
`;

const Menu = styled.div`
    background-color: #fff;
    color: #000;
    height: 120px;
    width: 80%;
    margin: 30px auto;
    border-radius: 20px;
    padding: 0 8px;
    transition: height 0.4s ease;

    &.more {
        height: 230px;
    }

    .grid {
        display:flex;
        margin-bottom: 10px;
    }

    .grid, .expanded {
        justify-content: space-between;
        transition: height 0.4s ease;
    }

    .expanded {
        height: 108px;
        display: none;
    }

    .expanded.enabled {
        display: flex;
    }

    div {
        display: inline-table;
        position: relative;
        margin-left: 9px;
    }

    label {
        font-weight: bold;
    }

    input, label, select {
        color: black;
        display: inline-table;
        font-size: 16.4px;
        margin-top: 18px;
        white-space:nowrap;
    }

    input[type=date] {
        margin-left: 15px;
        width: 235px;
    }

    .dateRow {
        width: 565px;
    }

    .dateHolder {
        margin-left: 6px;
    }

    #start {
        margin-left: 0px;
    }

    select {
        margin-left: 10px;
    }
    #sortSelect {
        width: 61px;
    }
    #orderSelect {
        width: 112.4px;
    }

    .toggle {
        width: 310px;
    }

    #toggle1 {
        display: none;
    }

    .toggleSlider {
        display: inline-block !important;
        width: 50px !important;
        height: 20px;
        background-color: #f00;
        top: 4px;
        margin-left: 60px;
        float: none;
        border-radius: 10px;
        cursor: pointer;
        transition: background 0.4s ease;
    }
    .toggleSlider.enabled {
        background-color: #0f0;
    }
    .toggleButton {
        width: 18px !important;
        height: 18px;
        background-color: #fff;
        border-radius: 50%;
        top: 1px;
        margin-left: 0 !important;
        left: 1px;
        transition: left 0.4s ease;
    }

    .toggleButton.enabled {
        left: 31px;
    }

    .titleFilter {
        width: 220px;
    }

    .titleFilter input {
        margin-left: 8px;
        width: 150px;
    }

    .buttonHolder {
        display: flex;
        width: 430px;
        justify-content: space-between;
        margin: auto;
    }

    .sort {
        width: 345px;
    }
    
    .orderlabel {
        margin-left: 20px;
    }

    .descFilter {
        width: 310px;
        position:relative;
    }

    .descFilter label {
        transform: translateY(-51px);
    }

    textarea {
        resize: none;
        position:relative;
        top: 17px;
        font-size: 16px;
        margin-left: 10px;
        height: 80px;
        width: 150px;
    }

    @media all and (max-width:1370px){

        &.more {
            height: 340px;
        }

        .expanded {
            height: 225px;
        }

        .expanded.enabled {
            display: block;
        }

        #toggle1 {
            display: block;
        }

        .expanded div {
            width: 410px;
            display: block;
            margin: auto;
        }

        #toggle0 {
            display: none;
        }

        .descFilter label {
            transform: translateY(-21px);
        }

        textarea {
            width: 299px;
            font-size: 14.5px;
            margin-left: 0;
            top: 0;
        }

        .toggleSlider {
            float: right;
            top: 21px;
            right: 3px;
        }

        select{
            width: auto;
            margin-left: 8px !important;
        }

        input[type=date] {
            width: 146px;
            margin-left: auto;
        }

        .sort, .dateRow, .titleFilter {
            width: auto;
        }

        .buttonHolder, .expanded div {
            width: 410px;
            display: flex;
            align-items: space-between;
            justify-content: space-between;
        }

        .dateHolder {
            width: auto !important;
            margin: 0 0 20px 0 !important;
            display: flex !important;
            justify-content: space-between;
            align-items: space-between;
        }
    }

    @media all and (max-width: 900px){

        width: 90%;
        padding: 0 4px;

        input, label, select {
            font-size: 14.5px;
            margin-top: 21px;
        }

        input[type=date] {
            margin-left: 13px;
            width: 131px;
        }

        #sortSelect {
            width: 57px;
        }
        #orderSelect {
            width: 99px;
        }
        textarea {
            width: 277px;
        }

        .toggleSlider {
            display: inline-table;
            right: 0;
        }

        .dateHolder {
            margin: 0 !important;
        }

        .descFilter {
            margin-top: 20px !important;
        }
    }

    @media all and (max-width: 600px){
        border-radius: 10px;
        height: 150px;

        input, label, select {
            font-size: 13.5px;
        }

        &.more {
            height: 380px;
        }

        .grid {
            display: block;
        }

        .sort, .titleFilter, .buttonHolder, .expanded div {
            margin: auto;
            display: flex;
            justify-content: space-between;
            align-content: space-between;
            width: 340px;
        }

        label {
            margin-bottom: 8px;
        }

        .titleFilter input, textarea {
            width: 255px;
        }
        input[type=date] {
            margin-left: 3px;
            width: 124px;
        }

        select {
            margin-left: 17px !important;
        }
    }
    @media all and (max-width: 400px){
        height: 160px;

        &.more {
            height: 422px;
        }

        .expanded {
            height: 260px;
            margin-left: 0;
        }

        .grid {
            top: 5px;
            width: 100%;
            margin-left: 0;
            margin-bottom: 12px;
        }
        .titleFilter, .expanded div {
            display: block;
            justify-content: start;
            align-items: start;
        }

        .sort, .titleFilter, .buttonHolder, .expanded div, .descFilter {
            width: 260px;
        }

        input {
            margin-top: 0 !important;
        }

        .titleFilter input {
            width: 257px;
            margin-left: 0;
        }

        label {
            display: block;
        }

        .toggle label {
            display: inline-block;
        }

        select {
            margin-left: 8px !important;
        }

        .descFilter label {
            transform: translateY(0);
        }

        textarea {
            width: 257px;
            top: 0;
        }

        input[type=date] {
            width: 122px;
            margin-left: auto;
        }
    }
`;

const SoftwareIcon = styled.img`
    width: 105px;
    margin-right: 20px;
    margin-bottom: 10px;
    transition: filter 0.4s ease;

    :hover, :focus {
        filter: brightness(124%);
    }

    @media all and (max-width: 900px){
        width: 80px;
    }
`;

const Videos = props => {

    //variables, constants

    let pageContent = <Spinner />;

    let softwareContent = null;

    let errorMessage = null;

    const { error, softwareError, modalOpen, modalClose, fetchSoftware, fetchVideos, resetError,
        resetSoftwareError, loading, videos, software, softwareLoading, wW, isModalOpen } = props;

    //state, refs

    const el = useRef(null);

    const [filter, setFilter] = useState({
        name: "",
        sortBy: "uploaded",
        order: "descending",
        startDate: "",
        endDate: "",
        sound: true,
        desc: ""
    });
    const [wrapperHeight, setWrapperHeight] = useState(null);
    const [loadTimeStamp, setLoadTimeStamp] = useState(null);
    const [more, setMore] = useState(false);
    const [toggleTO, setToggleTO] = useState(false);
    const [moreFinished, setMoreFinished] = useState(false);
    const [scrolledOnce, setSTBO] = useState(false);
    const [last, setLast] = useState(null);
    const [scrollValue, setScrollValue] = useState(null);

    //useEffect

    useEffect(() => {
        fetchVideos(null);
        setLast("videos");

        return () => {
            resetSoftwareError();
        }
    }, [resetSoftwareError, fetchVideos]);

    useEffect(() => {

        if (((error) && (last === "videos")) || ((softwareError) && (last === "software"))) {
            modalOpen();
        }
        else {
            modalClose();
        }

        return;
    }, [error, softwareError, modalClose, modalOpen, last]);

    useEffect(() => {

        if (scrolledOnce && !isModalOpen && software.length === 0 && !loading && !softwareError) {
            setLast("software");
            fetchSoftware();
        }

    }, [scrolledOnce, fetchSoftware, isModalOpen, softwareError, loading, software.length]);

    //methods

    const softwareFilter = filterString => {
        return software.filter(f => f.type === filterString)
            .map(s => {
                return (<a href={"https://" + s.url}
                    target="_blank"
                    key={s.title}
                    rel="noreferrer noopener">
                    <SoftwareIcon
                        src={"/img/" + s.img + ".png"}
                        loading="lazy"
                        title={s.title}
                        alt={s.title}
                    />
                </a>);
            });
    }

    const validateSearch = () => {
        if (filter.startDate && filter.endDate &&
            new Date(filter.startDate) >= new Date(filter.endDate)
        ) {
            props.throwError({ message: "The start date is more recent than the end date", userError: true });
        }
        else {
            setLast("videos");
            fetchVideos(filter);
        }
    }

    const saveHeight = height => {
        setWrapperHeight(height);
    }

    //modals

    if (error || softwareError) {

        if (last) {
            if (last === "software" && softwareError) {
                errorMessage = " " + convertError(softwareError);
            }
            else if (last === "videos" && error) {
                errorMessage = " " + convertError(error);
            }
        }
        if (error) {
            pageContent = <BodyErrorText>{convertError(error)}</BodyErrorText>;
        }
        if (softwareError) {
            softwareContent = <BodyErrorText>{convertError(softwareError)}</BodyErrorText>;
        }
    }

    if (!loadTimeStamp) setLoadTimeStamp(new Date());

    if (softwareLoading) {
        softwareContent = <Spinner />;
    }

    if (software.length > 0 && Array.isArray(software)) {
        softwareContent = (
            <FlexContainer>
                <FlexInner>
                    <h2>Suites</h2>
                    {
                        softwareFilter("suite")
                    }
                </FlexInner>
                <FlexInner>
                    <h2>Plugins</h2>
                    {
                        softwareFilter("plugin")
                    }
                </FlexInner>
            </FlexContainer>
        );
    }

    if (videos.length > 0 && videos !== "No results" && Array.isArray(videos)) {
        pageContent = (
            <Fragment>
                <div style={{ textAlign: "center", marginTop: "20px" }}>
                    {videos.map(v => {
                        return <Article key={v._id} id={v._id}>
                            <Link to={"/videos/popup?itemID=" + v._id}><img src={v.thumb}
                                loading="lazy"
                                alt={v.name}
                            />
                            </Link>
                            <h3>{v.name}</h3>
                            <p>{v.uploaded.split('T')[0]}</p>
                        </Article>
                    })}</div>
            </Fragment>
        );

        if (wrapperHeight - 50 < window.innerHeight && !scrolledOnce) {
            setSTBO(true);
        }
    }
    else if (videos === "No results") {
        pageContent = <BodyErrorText type="warning">There were no videos matching the above query</BodyErrorText>;
    }

    let errorPromptContent = (
        <Fragment>
            <Button clicked={() => {
                modalClose();
                if (wrapperHeight - 50 < window.innerHeight) {
                    setSTBO(true);
                }

            }}>Dismiss</Button>
            <Button clicked={() => {
                switch (last) {
                    case ("software"): fetchSoftware();
                        break;
                    default: fetchVideos(filter);
                }
            }}>Retry</Button>
        </Fragment>
    );

    if (error && error.userError) {
        errorPromptContent = <Button clicked={() => { modalClose(); resetError(); }}>Dismiss</Button>;
    }

    return (
        <Fragment>
            <Helmet>
                <title>Videos - Saddex Productions</title>
            </Helmet>
            <Switch>
                <Route path="/videos/popup"
                    render={() => <Popup {...props} items={videos} type="video" />}
                />
            </Switch>
            <Modal
                enabled={isModalOpen}
                error={error}
                errorMsg={errorMessage}
                details={"Could not load " + last + "!"}
            >
                {errorPromptContent}
            </Modal>
            <Wrapper
                locked={isModalOpen}
                enableScroll
                scrolled={e => {
                    const scrollTimeStamp = new Date();

                    if (!loadTimeStamp || scrollTimeStamp - loadTimeStamp < 200 ||
                        (scrollValue === e.target.scrollTop || !scrollValue || isModalOpen || loading)
                    ) {
                        if (!scrollValue) setScrollValue(e.target.scrollTop);
                        return;
                    }
                    else if (e.target.scrollTop !== scrollValue && scrollValue !== null) {
                        setScrollValue(e.target.scrollTop);
                    }
                    const isVisible = isInViewport(el.current);
                    if (!scrolledOnce && isVisible) {
                        setSTBO(true);
                    }
                }}
                passHeight={saveHeight}
            >
                <H1 text="Videos" />
                <P margin={wW > 900 ? "8px 10% 13px 10%" : "8px 5% 13px 5%"}>
                    This page contains my previous video work. The videos are hosted on Vimeo
                    and form a mix of university projects, amateur freelancing work and personal
                    experiments/hobby projects. Click on a video to learn more about it.
                </P>
                <Menu className={more && "more"}>
                    <form onSubmit={e => {
                        e.preventDefault();
                        validateSearch();
                    }}>
                        <div className="grid">
                            <div className="titleFilter">
                                <label>Title</label>
                                <input type="text" placeholder="Filter by title..."
                                    onChange={(e) => {
                                        setFilter({
                                            ...filter,
                                            name: e.target.value
                                        })
                                    }}
                                    value={filter.name}
                                />
                            </div>
                            <div className="sort">
                                <label>Sort by</label>
                                <select
                                    value={filter.sortBy}
                                    id="sortSelect"
                                    onChange={
                                        e => setFilter({ ...filter, sortBy: e.target.value })
                                    }>
                                    <option value="uploaded">Date</option>
                                    <option value="name">Title</option>
                                </select>
                                <label className="orderlabel">Order</label>
                                <select
                                    id="orderSelect"
                                    value={filter.order}
                                    onChange={e => setFilter({ ...filter, order: e.target.value })}>
                                    <option value="descending">Descending</option>
                                    <option value="ascending">Ascending</option>
                                </select>
                            </div>
                            <div className="toggle" id="toggle0">
                                <label>
                                    Videos with sound
                            </label>
                                <div className={!filter.sound ? "toggleSlider" : "toggleSlider enabled"} onClick={() => {
                                    setFilter({ ...filter, sound: !filter.sound });
                                }}>
                                    <div className={!filter.sound ? "toggleButton" : "toggleButton enabled"} value="true" />
                                </div>
                            </div>
                        </div>
                        <div className={moreFinished ? "expanded enabled" : "expanded"}>
                            <div className="toggle" id="toggle1">
                                <label>
                                    Videos with sound
                            </label>
                                <div className={!filter.sound ? "toggleSlider" : "toggleSlider enabled"} onClick={() => {
                                    setFilter({ ...filter, sound: !filter.sound });
                                }}>
                                    <div className={!filter.sound ? "toggleButton" : "toggleButton enabled"} value="true" />
                                </div>
                            </div>
                            <div className="dateRow">
                                <label>
                                    Date
                            </label>
                                <div className="dateHolder">
                                    <input type="date"
                                        min="2015-09-22"
                                        value={filter.startDate}
                                        id="start"
                                        onChange={e => {
                                            setFilter({
                                                ...filter,
                                                startDate: e.target.value
                                            })
                                        }}
                                    />
                                    <input type="date"
                                        min="2015-09-23"
                                        id="end"
                                        value={filter.endDate}
                                        onChange={e => {
                                            setFilter({
                                                ...filter,
                                                endDate: e.target.value
                                            })
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="descFilter">
                                <label>
                                    Description
                            </label>
                                <textarea maxLength="200"
                                    placeholder="Some description..."
                                    value={filter.desc}
                                    onChange={e => setFilter({ ...filter, desc: e.target.value })}>

                                </textarea>
                            </div>
                        </div>
                        <div className="buttonHolder">
                            <Button
                                type="button"
                                name="expand Or Contract"
                                clicked={() => {
                                    if (!toggleTO) {
                                        setMore(!more);
                                        setToggleTO(true);
                                        if (!more) {
                                            setTimeout(() => {
                                                setMoreFinished(true);
                                            }, 400);
                                        }
                                        else {
                                            setMoreFinished(false);
                                        }
                                        setTimeout(() => setToggleTO(false), 400);
                                    }
                                }}
                            >{!more ? "More..." : "Collapse"}</Button>
                            <Button type="submit" name="submit Search Form">
                                <FontAwesomeIcon icon={faSearch} />
                            </Button>
                        </div>
                    </form>
                </Menu>
                {pageContent}
                <div style={{ marginBottom: "40px" }}>
                    <H1 text="Software I have used" />
                    <P margin={wW > 900 ? "auto 10% 20px 10%" : "auto 5% 20px 5%"}>
                        This section does only include software I have decent knowledge in using, and excludes software I've only briefly tested.
                </P>
                    {softwareContent}
                </div>
                <span ref={el}></span>
            </Wrapper>
        </Fragment>
    )
};

const mapStateToProps = state => {
    return {
        loading: state.videos.loading,
        videos: state.videos.videos,
        error: state.videos.error,
        softwareLoading: state.software.loading,
        software: state.software.software,
        softwareError: state.software.error,
        isModalOpen: state.globalUI.modalOpen,
        wW: state.globalUI.windowWidth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchVideos: (filter) => { dispatch(actions.initVideos(filter)) },
        fetchSoftware: () => { dispatch(actions.initSoftware()) },
        throwError: (err) => { dispatch(actions.initVideosFail(err)) },
        resetError: () => { dispatch(actions.resetVideoError()) },
        resetSoftwareError: () => { dispatch(actions.resetSoftwareError()) },
        modalOpen: () => { dispatch(actions.modalOpen()) },
        modalClose: () => { dispatch(actions.modalClose()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Videos);