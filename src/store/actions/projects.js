import * as actionTypes from './actionTypes';

export const initProjects = (project_length) => {
    return {
        type: actionTypes.INIT_PROJECTS,
        project_length
    }
}

export const initProjectsStart = cancelToken => {
    return {
        type: actionTypes.INIT_PROJECTS_START,
        cancelToken: cancelToken
    }
}

export const initProjectsSuccess = data => {
    return {
        type: actionTypes.INIT_PROJECTS_SUCCESS,
        data: data
    }
}

export const initProjectsFail = (error,isCancel) => {

    return {
        type: actionTypes.INIT_PROJECTS_FAIL,
        error: error,
        isCancel: isCancel
    }
}

export const resetProjectsError = () => {
    return {
        type: actionTypes.RESET_PROJECTS_ERROR
    }
}