import * as actionTypes from './actionTypes';

export const initVideos = filter => {
    return {
        type: actionTypes.INIT_VIDEOS,
        filter: filter
    }
}

export const initVideosStart = cancelToken => {
    return {
        type: actionTypes.INIT_VIDEOS_START,
        cancelToken: cancelToken
    }
}

export const initVideosSuccess = data => {
    return {
        type: actionTypes.INIT_VIDEOS_SUCCESS,
        data: data
    }
}

export const initVideosFail = (error, isCancel) => {
    return {
        type: actionTypes.INIT_VIDEOS_FAIL,
        error: error,
        isCancel: isCancel
    }
}

export const resetVideoError = () => {
    return {
        type: actionTypes.RESET_VIDEO_ERROR
    }
}