import * as actionTypes from './actionTypes';

export const initSoftware = () => {
    return {
        type: actionTypes.INIT_SOFTWARE
    }
}

export const initSoftwareStart = cancelToken => {
    return {
        type: actionTypes.INIT_SOFTWARE_START,
        cancelToken: cancelToken
    }
}

export const initSoftwareSuccess = data => {
    return {
        type: actionTypes.INIT_SOFTWARE_SUCCESS,
        data: data
    }
}

export const initSoftwareFail = (error,isCancel) => {
    return {
        type: actionTypes.INIT_SOFTWARE_FAIL,
        error: error,
        isCancel: isCancel
    }
}

export const resetSoftwareError = () => {
    return {
        type: actionTypes.RESET_SOFTWARE_ERROR
    }
}