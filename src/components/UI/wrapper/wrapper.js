import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';
import Footer from '../../footer/footer';

const Wrapper = styled.div`
display: block;
top: 77px;
box-sizing: border-box;
height: calc(100vh - 77px);
position: relative;

.inner {
    margin: 0 10% 0 10%;
    width: 80%;
    background-color: rgba(0,0,0,0.92);
}

@media all and (max-width: 900px){
    .inner {
        width: 100%;
        margin-left: 0;
        margin-right: 0;
    }
}
`;

const WrapperFunction = props => {

    //useState, useRef

    const wrapRef = useRef(null);
    const [scrollTimeOut, setScrollTimeOut] = useState(false);

    //constants
    const {passHeight} = props;

    //useEffect

    useEffect(() => {
        if(wrapRef.current && wrapRef.current.scrollHeight && passHeight){
            passHeight(wrapRef.current.scrollHeight);
        }
    },[wrapRef, passHeight]);

    return (
        <Wrapper onScroll={(e) => {
                if(!scrollTimeOut && props.enableScroll){
                    setScrollTimeOut(true);
                    props.scrolled(e);
                    setTimeout(()=> setScrollTimeOut(false),20);
                }
            }} 
            style={{overflowY: props.locked ? "hidden" : "auto"}}
            ref={wrapRef}
            >
            <div className="inner">
                {props.children}
                <Footer noMail={props.noMail}/>
            </div>
        </Wrapper>
    )
};

export default WrapperFunction;