import { put } from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../shared/axios-instance';

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

export function* initProjectsSaga(action) {

    yield put(actions.initProjectsStart(source));
    try {
        const res = 
        yield axios
        .get(`/projects${action.project_length > 0 ? '?skip=' + action.project_length : ''}`,
        {
            cancelToken: source.token
        });
        yield put(actions.initProjectsSuccess(res.data));
    }
    catch (err) {
        yield put(actions.initProjectsFail(err,axios.isCancel));
    };
}