import { put } from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../shared/axios-instance';

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

export function* sendMailSaga(action) {

    yield put(actions.sendFormStart(source));
    if(action.data.formIsValid){
        try {
            yield axios.post('/send',{
                email: action.data.form.from.value,
                subject: action.data.form.subject.value,
                captcha: action.data.captcha,
                content: action.data.form.content.value
            },
            {
                cancelToken: source.token
            }
            );
            yield put(actions.sendFormSuccess());
        }
        catch (err) {
            yield put(actions.sendFormFail(err,axios.isCancel));
        };       
    }
}