import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as actions from '../../store/actions/index';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import ReCAPTCHA from "react-google-recaptcha";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import {checkEmpty, convertError} from '../../shared/utility';

import H1 from '../../components/UI/headline/headline';
import Wrapper from '../../components/UI/wrapper/wrapper';
import Input from '../../components/UI/input/input';
import Button from '../../components/UI/button/button';
import Modal from '../../components/UI/modal/modalPackage';
import Spinner from '../../components/UI/spinner/spinner';
import BodyStatusText from '../../components/UI/bodyErrorText/bodyErrorText';

const StyledForm = styled.form`
    width: 80%;
    display: block;
    margin: auto auto 40px auto;

    label {
        font-size: 18px;
        font-weight: bold;
        display: inline-table;
        margin: 15px auto 2px auto;
    }

    .specialFlex {
        display: flex;
        justify-content: space-between;
    }

    .specialFlex .el {
        margin-top: 25px;
    }

    .smallError {
        color: #f00;
        font-weight: bold;
        font-size: 14px;
    }

    textarea {
        resize: vertical
    }

    @media all and (max-width: 900px){
        .specialFlex {
            display: block;
        }
    }
`;

const Mail = props => {

    //variables, constants, state

    let emailFormContent;
    let allEmptyChanged;

    const { loading, error, reset, content, emptyForm } = props;
    
    const [allEmpty,setAllEmpty] = useState(checkEmpty(content.form,true));
    const [errorMsg, setErrorMsg] = useState(null);
    const [captchaValue, setCaptcha] = useState(null);
    const [redir, setRedir] = useState(false);
    const [sent, setSent] = useState(false);
    const formElements = [];

    //methods

    useEffect(() => {
        if(allEmpty){
            emptyForm();
        }

        if ((!redir && !sent && !error) || (allEmpty && error)) {
            reset();
        }
        else if (!error && !loading) {
            setTimeout(() => {
                setRedir(true);
            }, 2000);
        }
        if (error) {
            setErrorMsg(convertError(error));
        }
    }, [sent, loading, redir, error, reset, content.form,allEmpty, emptyForm]);

    const captchaChange = value => {
        setCaptcha(value);
    }

    for (let key in props.content.form) {
        formElements.push({
            id: key,
            config: props.content.form[key]
        });
    }

    if (!redir) {
        emailFormContent = (
            <Fragment>
                <H1 text="Contact" />
                <StyledForm
                    onKeyUp={
                        e => {
                            if (e.keyCode === 13) {
                                props.setTouched();
                                reset();
                                if (!props.content.formIsValid) {
                                    e.preventDefault();
                                    setErrorMsg("Please control entered information");
                                }
                                else if (props.content.formIsValid && captchaValue === null) {
                                    e.preventDefault();
                                    setErrorMsg("Please select captcha");
                                }
                            }
                        }
                    }
                    onSubmit={(e) => {
                        e.preventDefault();

                        if (props.content.formIsValid && captchaValue !== null) {
                            props.onSubmitHandler({
                                ...props.content,
                                captcha: captchaValue
                            });
                            setSent(true);
                        }
                    }}>
                    {
                        formElements.map(fC => (
                            <div key={fC.config.title}>
                                <label htmlFor={fC.config.title}>{fC.config.title}</label>
                                <Input
                                    name={fC.config.title}
                                    elementType={fC.config.elementType}
                                    elementConfig={fC.config.elementConfig}
                                    value={fC.config.value}
                                    invalid={!fC.config.valid}
                                    touched={fC.config.touched}
                                    keyup={e => {
                                        e.preventDefault();
                                        e.stopPropagation();
                                    }}
                                    shouldValidate={fC.config.validation}
                                    changed={(e) => {
                                        props.onChangeHandler(e, fC.id);
                                    }} />
                            </div>
                        ))
                    }
                    <div className="specialFlex">
                        <div className="el">
                            <ReCAPTCHA
                                sitekey="6Leh0mMUAAAAAPEzzptA06r0xL5rbIcPwxgIHu1u"
                                onChange={captchaChange}
                                onExpired={() => setCaptcha(null)}
                                onErrored={() => setCaptcha(null)}
                            />
                        </div>
                        <div className="el">
                            <Button
                                name="email Form Submit"
                                type="submit"
                                disabled={!props.content.formIsValid || !captchaValue}
                                passStyle={{ height: "76px" }}
                            >
                                <FontAwesomeIcon icon={faPaperPlane} />
                            </Button>
                        </div>
                    </div>
                    <p className="smallError">{errorMsg}</p>
                </StyledForm>
            </Fragment>
        );
    }
    else {
        emailFormContent = <Redirect to="/" />;
    }

    allEmptyChanged = (checkEmpty(content.form,allEmpty) !== allEmpty);

    if(allEmptyChanged){
        setAllEmpty(checkEmpty(content.form,allEmpty));
    }

    if (props.loading) {
        emailFormContent = (
            <div style={{ height: "calc(100vh - 100px)", display: "flex", alignContent: "center", justifyContent: "center" }} >
                <Spinner />
            </div >
        );
    }

    if (props.success && !redir) {
        emailFormContent = (
            <div style={{ display: "flex", alignContent: "center", justifyContent: "center" }}>
                <BodyStatusText custom={{ padding: "calc(50vh - 200px) 0" }} type="success">Message successfully sent</BodyStatusText>
            </div>
        );
    }

    return (
        <Fragment>
            <Helmet>
                <title>Contact - Saddex Productions</title>
            </Helmet>
            <Modal
                enabled={props.modal}
                errorMsg={errorMsg}
                details="The form couldn't be sent!"
            >
                <Button clicked={() => { props.reset(); setCaptcha(null); }}>Dismiss</Button>
                <Button clicked={() => {
                    props.onSubmitHandler({
                        ...props.content,
                        captcha: captchaValue
                    });
                    setSent(true);
                }}>Retry</Button>
            </Modal>
            <Wrapper noMail>
                {emailFormContent}
            </Wrapper>
        </Fragment>
    );
};

const mapStateToProps = state => {
    return {
        loading: state.form.loading,
        modal: state.form.modal,
        content: state.form.content,
        success: state.form.success,
        error: state.form.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onChangeHandler: (e, id) => dispatch(actions.updateForm(e, id)),
        onSubmitHandler: (data) => dispatch(actions.initFormSend(data)),
        reset: () => dispatch(actions.reset()),
        setTouched: () => dispatch(actions.setTouched()),
        emptyForm: () => dispatch(actions.emptyForm())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Mail);