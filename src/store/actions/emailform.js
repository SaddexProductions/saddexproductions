import * as actionTypes from './actionTypes';

export const updateForm = (e,id) => {
    return {
        type: actionTypes.UPDATE_FORM,
        e: e,
        id: id
    }
}

export const setTouched = () => {
    return {
        type: actionTypes.SET_TOUCHED
    }
}

export const initFormSend = form => {
    return {
        type: actionTypes.INIT_FORM_SEND,
        data: form
    }
}

export const sendFormStart = cancelToken => {

    return {
        type: actionTypes.SEND_FORM_START,
        cancelToken: cancelToken
    }
}

export const sendFormSuccess = () => {
    return {
        type: actionTypes.SEND_FORM_SUCCESS
    }
}

export const sendFormFail = (error,isCancel) => {
    return {
        type: actionTypes.SEND_FORM_FAIL,
        error: error,
        isCancel: isCancel
    }
}

export const reset = () => {
    return {
        type: actionTypes.RESET
    }
}

export const emptyForm = () => {
    return {
        type: actionTypes.EMPTY_FORM
    }
}